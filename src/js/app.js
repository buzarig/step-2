const burgerOpen = document.querySelector(".burger-menu");
const navList = document.querySelector(".nav__list");
const navLink = document.querySelectorAll(".nav__link");
const navLinkArr = [...navLink];
const borderLink = document.querySelectorAll(".border__link");
const borderLinkArr = [...borderLink];
let counter = 0;

for (let i = 0; i < navLinkArr.length; i++) {
  navLinkArr[i].addEventListener("focus", () => {
    navLinkArr.forEach((e) => {
      e.classList.remove("active");
    });
    borderLinkArr.forEach((b) => {
      b.classList.remove("active");
    });
    navLinkArr[i].classList.add("active");
    borderLinkArr[i].classList.add("active");
  });
}

burgerOpen.addEventListener("click", function () {
  burgerOpen.classList.toggle("active");
  navList.classList.toggle("active");
});

document.addEventListener("click", function (e) {
  const target = e.target;
  const its_menu = target == navList || navList.contains(target);
  const its_btnMenu = target == burgerOpen;
  const menu_is_active = navList.classList.contains("active");

  if (!its_menu && !its_btnMenu && menu_is_active) {
    navList.classList.toggle("active");
  }
});
